import operator

ex_dict = {1: 2, 3: 4, 4: 3, 2: 1, 0: 0}

asc = dict(sorted(ex_dict.items(), key=operator.itemgetter(1)))
print('Ascending order by value : ',asc)

desc = dict( sorted(ex_dict.items(), key=operator.itemgetter(1),reverse=True))
print('Descending order by value : ',desc)
