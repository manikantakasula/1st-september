import operator

ex_dict = {1: 2, 3: 4, 4: 3, 2: 1, 0: 0}

asc = dict(sorted(ex_dict.items(), key=operator.itemgetter(0)))
print('Ascending order by key : ',asc)
 
desc = dict( sorted(ex_dict.items(), key=operator.itemgetter(0),reverse=True))
print('Descending order by key : ',desc)
